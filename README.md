### file_header
Sublime text plugin to insert header at the top of the current file

#####Install (Linux)

1. Download from GitLab.<br>
<br>
2. Copy the file named `add_header.py` to your `/home/[user]/.config/sublime-text-3/Packages/User` directory.<br>
<br>
3. Open to modify the file named `Default (Linux).sublime-keymap`<br>
(located into your `/home/[user]/.config/sublime-text-3/Packages/User` directory),<br>
and add the lines below :<br>
```
[
  {"keys": ["ctrl+shift+h"], "command": "add_header" },
]
```
<br>
OK, Save the file and it's done. No need to quit, it's already available. <br>
Just press `CTRL-SHIFT-h` at the top of the file you're editing and voilà.
(this shortcut can be changed - see Preferences/Keybinding Sublime Text menu).<br>
<br>
See the example section below.
<br>
#####Example of file header
<br>
Let's say your source file is named *`add_header.py`*,<br>
its path as *`/home/[user]/.config/sublime-text-3/Packages/User`*,<br>
and your yawning morning day is the *`August, 22nd, 2016`*.<br>
<br>
You're ready to insert a file header !<br>
(this kind of section is placed at the top of the source file).<br>
<br>
So, set the cursor at the right place and press `CTRL-SHIFT-h` to insert the following header just in place:<br>
> \#-----------------------------------------------------------------<br>
> \# file.....: add_header.py<br>
> \# path.....: /home/io/.config/sublime-text-3/Packages/User<br>
> \# date.....: 20160822<br>
> \# author...: ioldx37<br>
> \# object...: <br>
> \# depends..: none<br>
> \#-----------------------------------------------------------------<br>

Of course, you may change the author's name here with your favorite nickname:<br>
*line 23*: 		```author    = "# author...: ioldx37\n"```
<br>
<br>
You will be probably interested about the next line (see the weird '$1'...):<br>
*line 24*: 		```theobject = "# object...: $1\n"```<br>
This '$1' means will set the cursor location directly at this location to let you enter the appropriate sentence..
