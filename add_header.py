#-----------------------------------------------------------------
# file.....: add_header.py
# path.....: /home/io/.config/sublime-text-3/Packages/User
# date.....: 20160822
# author...: LDx
# object...: to add a file header at the top of the current file
# depends..: none
#-----------------------------------------------------------------


import datetime, getpass
import sublime, sublime_plugin
import os.path
class AddHeaderCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		fullname  = self.view.file_name()
		thepath   = os.path.dirname(fullname)
		thefile   = os.path.basename(fullname)
		cm        = "#"
		dotline   = cm + "-----------------------------------------------------------------\n"
		file      = cm + " file.....: " + thefile + "\n"
		path      = cm + " path.....: " + thepath + "\n"
		datum     = cm + " date.....: " + datetime.date.today().strftime("%Y%m%d") + "\n"
		author    = cm + " author...: ioldx37\n"
		theobject = cm + " object...: $1\n"
		depends   = cm + " depends..: none\n"
		header    = dotline + file + path + datum + author + theobject + depends + dotline
		self.view.run_command("insert_snippet", { "contents": "%s" %  header } )